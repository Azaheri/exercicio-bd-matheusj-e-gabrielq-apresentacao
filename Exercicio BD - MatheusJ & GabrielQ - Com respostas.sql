DROP DATABASE IF EXISTS exercicio;
CREATE DATABASE exercicio;
USE exercicio;
#Desativando a opção de update/delete seguros do mysql
SET SQL_SAFE_UPDATES = 0;

CREATE TABLE Funcionarios(
id_funcionario BIGINT PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(120),
departamento char(3),
funcao VARCHAR(30),
salario DECIMAL(10,2),
admissao DATE
);

INSERT INTO Funcionarios(nome, departamento, funcao, salario, admissao) VALUES 
('Rubens Miccjiv', 'Psq', 'Cientista', 7633.21, '2012-03-04'),
('Maycon Northman', 'Psd', 'Vice Presidente', 11458.17, '2012-02-22'),
('Lincoln Zedd', 'Psq', 'Cientista', 7421.22, '2012-03-04'),
('Sara Raganhildis', 'Ger', 'Gerente', 9655.87, '2012-02-23'),
('Dulce Lauressa', 'Lpz', 'Faxina', 2894.49, '2013-01-09'),
('Katell Sigfrid', 'Lpz', 'Faxina', 2731.08, '2013-01-09'),
('Manuela Yannic', 'Ctb', 'Contábil', 4901.05, '2012-02-24'),
('Eda Esmaralda', 'Ctb', 'Contábil', 4251.00, '2012-03-22'),
('Cornélia Ketevan', 'Psq', 'Cientista', 8004.62, '2012-08-09'),
('Lauryn Kelda', 'RH', 'Gestão RH', 5826.47, '2013-08-09'),
('Nada Milica', 'Psd', 'Presidente', 12785.33, '2012-02-22'),
('Pallas Arlene', 'Ctb', 'Contábil', 4591.01, '2013-08-09'),
('Flaviana Helēna', 'Psq', 'Cientista', 7734.66, '2012-04-01'),
('Geovanni Júnior', 'Psq', 'Estágio', 2434.66, '2017-06-05'),
('Marcos Siqueira', 'Psq', 'Estágio', 2434.66, '2017-06-05'),
('Leoncio Steven', 'Psq', 'Estágio', 2434.66, '2017-06-05'),
('Luan Duniqq', 'Psq', 'Estágio', 2434.66, '2017-06-05');

#Mostra todos os dados por derpartamento para facilitar.
SELECT * FROM Funcionarios ORDER BY departamento;

#Mostra a quantidade de departamentos
SELECT departamento, COUNT(Funcionarios.departamento) AS Quantidade FROM Funcionarios GROUP BY departamento;

#Mostra o nome, função e salário de cada funcionário
SELECT nome, funcao, salario FROM Funcionarios GROUP BY salario;

#Tabela dos dados movidos da tabela original
CREATE TABLE dados_mortos(
id_funcionario BIGINT PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(120),
departamento char(3),
funcao VARCHAR(30),
salario DECIMAL(10,2),
admissao DATE
);

#####################################################################################################################################################################################
#																				Respostas																							#
#####################################################################################################################################################################################
#																																													#
#1																																													#
insert into dados_mortos select id_funcionario, concat('[Demitido]', nome), departamento, funcao, salario, admissao from Funcionarios where id_funcionario = 7;						#
delete from Funcionarios where id_funcionario = 7;																																	#
#																																													#
#2																																													#
insert into dados_mortos select id_funcionario, concat('[Falecido]', nome), departamento, funcao, salario, admissao from Funcionarios where id_funcionario = 1;						#
delete from Funcionarios where id_funcionario = 1;																																	#
#																																													#
#3																																													#
insert into dados_mortos select id_funcionario, concat('[Estágio finalizado]', nome), departamento, funcao, salario, admissao from Funcionarios where funcao like 'Est%';			#
delete from Funcionarios where funcao like 'Est%';																																	#
#																																													#
#4																																													#
insert into dados_mortos select id_funcionario, concat('[Mudança de nome]', nome), departamento, funcao, salario, admissao from Funcionarios where nome like '%Laure%';				#
UPDATE Funcionarios SET nome = REPLACE(nome, 'Lauressa', 'Laurezza') WHERE nome LIKE '%Laure%';																						#
#####################################################################################################################################################################################

#Verificar se as operações foram feitas como deveriam.
select * from Funcionarios;
select * from dados_mortos;

#Apenas para testes - Deleta as informações da tabela sem deletar a tabela em si.
/*truncate dados_mortos;
truncate Funcionarios;*/